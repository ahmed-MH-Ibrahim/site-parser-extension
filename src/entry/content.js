import $ from "jquery";
import swal from 'sweetalert2';
import  '../assets/css/content.css';
import { createApp } from 'vue';
import CommandsPopup from '../view/commands.vue';

//global vars
var url = window.location.href;
var currentSelection,storedSelection,currentRange,storedRange;
var rangySelection,selectedElement;
var selectableElements = [], areElementsLoaded = false;
var currentMousePos = { x: -1, y: -1 };
var popupYMargin = 25;
var textRequested = '';
var textHighlighted = false;
var hasSelectionChanged = false;
var maxSringLimit = 1000000;
var isPopupClicked = false;
var commandsPopupApp;
var currentTextElement; //stores current active text element on a webpage

//detect storage changes to update settings live
chrome.storage.onChanged.addListener(function(changes, namespace) {

     //handle translateion settings view changes to reflect on translation results view
     if(changes.isExtensionEnabled!= undefined && commandsPopupApp.isRatingEnabled != changes.isExtensionEnabled.newValue )
     {
     		commandsPopupApp.isExtensionEnabled = changes.isExtensionEnabled.newValue;
     }
});

$(document).ready(function(){

	//appending stylesheets
	$('body').append('<link rel="stylesheet" type="text/css" href="'+chrome.runtime.getURL('css/content.css')+'">');

	$("body").append("<div id='site-parser-ui-popup-container' style='display:none;' class='site-parser-ui-popup-container'></div>");
	//append translate popup vue to site dom
	commandsPopupApp = createApp(CommandsPopup).mount('#site-parser-ui-popup-container');

	//handle text select
	document.addEventListener('select', handleSelection);



	//handle mouse coordinates
	$(document).mousemove(function(event) 
	{
		if(!textHighlighted)
		{
		   currentMousePos.x = event.pageX;
		   currentMousePos.y = event.pageY;
		}
	});

  	//detect key presses
	$( document ).keydown(async function(event) {
	  	if (commandsPopupApp.isExtensionEnabled && event.key === 'q')
		{
			if(event.ctrlKey)
			{
				commandsPopupApp.redirectNextSite();

			}
		}
	});	

  	//handle mouse clicks to remove popup to improve user experience
  	$(document).on('click', function(e){

		if(checkPopupClicked(e))
			return;				

		closePopup();

		handleSelection();      
	});

	//reset on mousedown click
	$(document).on("mousedown",function(e){
		if(checkPopupClicked(e))
			return;				

		closePopup();
	});

	
	$("#site-parser-popup-container, #site-parser-ui-popup-container").click(function(event){
		isPopupClicked = true;
		//event.stopPropagation();
	})

	//handle keypress to hide ui to improve user experience
	$(document).keydown(function() {
	  	closePopup();
	});

	//handle scrolling to hide ui to improve user experience
	$('body').on('mousewheel', function (e) {
			if(checkPopupClicked(e))
				return;

	    closePopup(true);
  	});

});

//function to handle popup click detection
function checkPopupClicked(e)
{
	if($(e.target).hasClass("site-parser-btn") || $(e.target).hasClass("site-parser-ui-popup-container") || $(e.target).parents(".site-parser-ui-popup-container").length)
		return true;

	return false;
}

function handleSelection(event) {

	chrome.storage.local.get(['isExtensionEnabled'],async function(obj){

		if(obj == undefined || obj.catchyEnabled == undefined || obj.catchyEnabled == true)
		{
			if(isPopupClicked && $("#site-parser-popup-container").is(":visible"))
			{
				isPopupClicked = false;
				return;
			}

			//handling case after text replacement is selected when popup is closed
			if(!$("#site-parser-ui-popup-container").is(":visible"))
				isPopupClicked = false;

			var selectionEl = $("#site-parser-ui-popup-container").get(0);

			textHighlighted = true;
			resetFlags();

			var isEmptySelection = true;

			var tempText = "", sel;
			if (window.getSelection) {
		        tempText = "" + window.getSelection();
		    } else if ( (sel = document.selection) && sel.type == "Text") {
		        tempText = sel.createRange().text;
		    }

			if(tempText && tempText.trim() != '')
			{
				isEmptySelection = false;
				//selectedElement = selectableElements[i];
				textRequested = tempText.trim();
				commandsPopupApp.textRequested = textRequested;

			}

			if(isEmptySelection)
			{
				selectedElement = undefined;
				textRequested = "";
				commandsPopupApp.textRequested = textRequested;

				//hide popup
				closePopup();
				isPopupClicked = false;

				return;
			}

			console.log(textRequested);

			//check if text larger than 200, update range
			if(textRequested.length > maxSringLimit)
			{
				//show warning for only 200 char limit
				showMaxLimitWarning();
				return;
			}
			

			if(event == undefined)
			{
				if(currentMousePos.x + $("#site-parser-ui-popup-container").width() < $(window).width())
					selectionEl.style.left = currentMousePos.x + "px";
				else
					selectionEl.style.left = (currentMousePos.x - $("#site-parser-ui-popup-container").width()) + "px";

				if(currentMousePos.y + $("#site-parser-ui-popup-container").height()< $(window).height())
			    	selectionEl.style.top = (currentMousePos.y + popupYMargin)+ "px";
			    else
			    	selectionEl.style.top = ((currentMousePos.y + popupYMargin) - $("#site-parser-ui-popup-container").height()) + "px";
			}
			else
			{
				selectionEl.style.left = event.pageX + "px";
			  selectionEl.style.top = (event.pageY + popupYMargin)+ "px";

			} 

			$("#site-parser-ui-popup-container").show(); 	    
			
		}
		
	});

	
}

function resetFlags(){
	hasSelectionChanged = true;
}

function showMaxLimitWarning(){
	swal.fire({
    title: "Character limit is "+maxSringLimit,
    text: "Max number of characters exceeded",
    icon:  "warning",
    timer: 3000,
    position: 'top-end',
  	backdrop: false,
  });
}

function closePopup()
{
	$("#site-parser-popup-container").hide();
	textHighlighted = false;

	//closing popup
	$("#site-parser-ui-popup-container").hide();
	//commandsPopupApp.closePopup();
}


