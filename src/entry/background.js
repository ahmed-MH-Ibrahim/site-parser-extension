

//hanlde on install event
chrome.runtime.onInstalled.addListener(function() {

    //handle right click menu
    chrome.contextMenus.create({
    	id: "site-parser-image-url",
      	title: 'Add Image url',
      	contexts: ["image"]
    }, function(){})
});

chrome.contextMenus.onClicked.addListener(function(info, tab) {

 	//handle context menu actions
 	if(info.menuItemId == "site-parser-image-url" )
    {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
	        chrome.tabs.sendMessage(tabs[0].id, {"command": "image-src-command", url:info.srcUrl}, function(response) {
	            console.log(response);
	        });
	    });
    }
})


//handle extension events
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
{
  if(request.command == "redirect")
  {    
    chrome.tabs.update(sender.tab.id, {url: request.redirect});
  }
});

